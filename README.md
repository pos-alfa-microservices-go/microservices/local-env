# local-env

Configurações locais para executar os serviços 

## Pré requisitos

Instalação necessárias:
- Golang [1.17+](https://go.dev/doc/install)
- Docker [18.02.0+](https://docs.docker.com/engine/install/)

## Execução 

1. Execute o docker compose `docker-compose up` para inicializar os bancos de dados e RabbitMQ
2. Execute `go run main.go` para inicializar cada um dos serviço.
 - `user % go run main.go`  
 - `product % go run main.go`
 - `customer % go run main.go`
 - `order % go run main.go`
 - `order-receiver % go run main.go`
 - `order-aggregator % go run main.go`


